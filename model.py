import itertools
import matplotlib.pyplot as plt

import torch
import torchvision
import torch.nn as nn
import torchvision.datasets as dsets
import torchvision.transforms as transforms
from torch.autograd import Variable

IMG_X_DIM = 28
IMG_Y_DIM = 28
IMG_FLAT_DIM = IMG_X_DIM * IMG_Y_DIM

GEN_INPUT_DIM = 64

class Generator(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(GEN_INPUT_DIM, 256),
            nn.LeakyReLU(0.2, inplace=True),

            nn.Linear(256, 512),
            nn.LeakyReLU(0.2 ,inplace=True),

            nn.Linear(512, 1024),
            nn.LeakyReLU(0.2 ,inplace=True),

            nn.Linear(1024, IMG_FLAT_DIM),
            nn.Tanh()
        )

    def forward(self, x):
        return self.model(x.view(x.size(0), GEN_INPUT_DIM))


class Discriminator(nn.Module):
    def __init__(self):
        super().__init__()
        self.model = nn.Sequential(
            nn.Linear(IMG_FLAT_DIM, 1024),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Dropout(0.3),

            nn.Linear(1024, 512),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Dropout(0.3),

            nn.Linear(512, 256),
            nn.LeakyReLU(0.2, inplace=True),
            nn.Dropout(0.3),

            nn.Linear(256, 1),
            nn.Sigmoid()
        )
        
    def forward(self, x):
        out = self.model(x.view(x.size(0), IMG_FLAT_DIM))
        out = out.view(out.size(0), -1)
        return out

class TrainingManager(object):
    def __init__(self, train_loader, generator, gen_optimizer,
                       discriminator, disc_optimizer, loss_func):
        self.train_loader = train_loader
        self.generator = generator
        self.gen_optimizer = gen_optimizer
        self.discriminator = discriminator
        self.disc_optimizer = disc_optimizer
        self.loss_func = loss_func

        self.test_noise = Variable(torch.randn(16, GEN_INPUT_DIM).cuda())

    def _train_epoch(self, make_plot=False, plot_path=None):
        for images, _ in self.train_loader:
            # `images` has dimension batch_size x 28 x 28

            # Get the real images and the real labels
            # (Labels are all 1, since images are real)
            real_images = Variable(images.cuda())
            real_labels = Variable(torch.ones(real_images.size(0)).cuda())

            # Train the discriminator
            ##########################################################################

            # (Fake labels are all 0)
            noise = Variable(torch.randn(real_images.size(0), GEN_INPUT_DIM).cuda())
            fake_images = self.generator(noise)
            fake_labels = Variable(torch.zeros(real_images.size(0)).cuda())

            self.discriminator.zero_grad()
            disc_on_real = self.discriminator(real_images)
            disc_real_loss = self.loss_func(disc_on_real, real_labels)

            disc_on_fake = self.discriminator(fake_images)
            disc_fake_loss = self.loss_func(disc_on_fake, fake_labels)

            total_disc_loss = disc_real_loss + disc_fake_loss
            total_disc_loss.backward()
            self.disc_optimizer.step()

            # Train the generator
            ##########################################################################

            noise = Variable(torch.randn(real_images.size(0), GEN_INPUT_DIM).cuda())
            fake_images = self.generator(noise)
            disc_scores = self.discriminator(fake_images)

            # Use `real_labels` (i.e. all 1s) because the generator wants to fool the
            # discriminator into thinking all its generated images are real
            self.generator.zero_grad()
            gen_loss = self.loss_func(disc_scores, real_labels)
            gen_loss.backward()
            self.gen_optimizer.step()

        if make_plot:
            test_images = self.generator(self.test_noise)
            size_figure_grid = 
            fig, ax = plt.subplots(size_figure_grid, size_figure_grid, figsize=(6, 6))
            for i, j in itertools.product(range(size_figure_grid), range(size_figure_grid)):
                ax[i,j].get_xaxis().set_visible(False)
                ax[i,j].get_yaxis().set_visible(False)

            for k in range(test_images.shape(0)):
                i = k // 4
                j = k %  4
                ax[i,j].cla()
                ax[i,j].imshow(test_images[k,:].data.cpu().numpy().reshape(28, 28), cmap='Greys')
            plt.savefig(plot_path)

        return total_disc_loss, gen_loss, disc_on_real.data.mean(), disc_on_fake.data.mean()


    def train(self, epochs=200, make_plot=False):
        for epoch in range(epochs):
            disc_loss, gen_loss, d_x, d_gen_z = self._train_epoch(make_plot, plot_path=f'results/epoch-{epoch:03d}')
            print(f'Epoch {epoch:03d}, disc_loss {disc_loss:0.2f}, gen_loss {gen_loss:0.2f}')
            print(f'                   D(x) {d_x}, D(G(z)) {d_gen_z})')

if __name__ == '__main__':
    transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(mean=(0.5, 0.5, 0.5), std=(0.5, 0.5, 0.5))
    ])

    train_dataset = dsets.MNIST(root='./data/', train=True, download=True, transform=transform)
    train_loader = torch.utils.data.DataLoader(train_dataset, batch_size=128, shuffle=True)

    generator = Generator()
    discriminator = Discriminator()
    loss = nn.BCELoss()
    learning_Rate = 0.0002
    disc_optimizer = torch.optim.Adam(discriminator.parameters(), lr=learning_rate)
    gen_optimizer = torch.optim.Adam(generator.parameters(), lr=learning_rate)

    training_manger = TrainingManager(
        train_loader=train_loader,
        generator=generator,
        gen_optimizer=gen_optimizer,
        discriminator=discriminator,
        disc_optimizer=disc_optimizer,
        loss_func=loss,
    )

    training_manager.train(epochs=10)